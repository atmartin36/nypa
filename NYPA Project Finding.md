---
title: "NYPA Project Finding"
date: March 2017
output: word_document
---
## Objective
The purpose of this project is to find a meaningful project for NYPA. This report explores Smart Wires' impact on increasing transfer limits across Mosses South and UPNY-SENY. The Moses South interface connects upstate NY hydro, wind, and Canadian import to the rest of NY. The UPNY-SENY interface connects Upstate New York to South East New York. Transmission considered for deployments is not limited to NYPA owned.

## Assumptions
* Transfer Analysis was conducted via DC linear approximation
* Phase Shifting Transformers are fixed flow pre-contingency, fixed angle post-contingency
* Base case is based on the 2016 FERC-715 series 2021 summer peak case and is conditioned similar to NYISO's methodology
  * Update generation 'levelized' to have same pgen/pmax ratios
  * Nuclear generation maxed
  * Wind generation offline
  * Fitzpatrick nuclear facility is online

## Scenarios
* Summer Peak 2021
* Summer Peak 2021, Fitzpatrick offline
* Summer Peak 2021, 300 MW St Lawrence import over PSTs
* Light Load 2021 with Wind at 80%


## Historical Constraints
Below are the historical constraints as shown in NYISO's Annual Area Transmission Review. The transfer analysis conducted for the NYISO's ATR is not specifically  intended to identify real world operating limits but rather is used as a metric to compare system changes from year to year. For this reason, the transfer limits identified may not reflect real system constraints.

***Moses South***

| Study Year | Constraint                         | Interface Limit [MW] |
| :--------: | :--------------------------------- | :------------------: |
| 2005       | Adirondack - Porter 230 kV         | 1566                 |
| 2008       | Moses - Adirondack 230 kV          | 2342                 |
| 2010       | Moses - Adirondack 230 kV          | 2475                 |
| 2013       | Moses - Adirondack 230 kV          | 2500                 |
| 2014       | Moses - Adirondack 230 kV          | 2350                 |
| 2015       | Browns Falls - Taylorville 115 kV  | 2350                 |

***UPNY - SENY***

| Study Year | Constraint                      | Interface Limit [MW] |
| :--------: | :------------------------------ | :------------------: |
| 2005       | Leeds - Pleasant Valley 345 kV  | 4575                 |
| 2008       | Leeds - Pleasant Valley 345 kV  | 5425                 |
| 2010       | Leeds - Pleasant Valley 345 kV  | 5250                 |
| 2013       | Leeds - Pleasant Valley 345 kV  | 5125                 |
| 2014       | CPV Valley - Rock Tavern 345 kV | 5075                 |
| 2015       | CPV Valley - Rock Tavern 345 kV | 5075                 |

Notice that in the past few years the limiting constraints have shifted. For this reason, this study explores Smart Wires' influence on all potential constraints.

## Study Methodology
The methodology used in this study is intended to construct the relationships a deployment has on each relevant constraint. Transfer analysis was conducted via TARA's proportional transfer application. Deployments were considered for an array of facilities thought to have impact on a potential constraint. The MW impact each deployment option had on each relevant constraint's transfer limitation was observed and recorded.


## Results
The results described in this section are high level and show the general influence Smart Wires may be able to provide. More detailed results and deployment descriptions are appended. It's important to focus on the constraint impacts Smart Wires can provide rather than the actual interface limits. The limits derived in this study are not intended to reflect actual operating limits.

***Moses South***
As shown by the historical constraints, the Moses - Adirondack 230 kV and Browns Falls - Taylorville 115 kV constraints have been really close to each other in the recent past. Both constraints are driven by the same limiting contingency which is loss of the HQ - NY import. A Smart Wires deployment could be utilized to relieve either constraint, however since the same contingency drives both constraints, post-contingent switching that alleviates one constraint will exacerbate another.

Roughly 13 Ohms could be used to add 100 MW of transfer headroom to the 115 kV constraint (Deployment *MS1* below). This would involve a parallel deployment on two circuits between Browns Falls and Taylorville and would potentially reduce transfer limits associated with 230 kV constraints by 20 MW. On the 230 kV constraints, 13 ohms would alleviate roughly 50 MW of transfer headroom (Deployment *MS2* below). This would involve a parallel deployment on the two 230 kV circuits between Moses and Adirondack and would potentially reduce transfer limits associated with 115 kV constraints by 25 MW.  

The table below displays the described deployments and shows their impact on Moses South transfer limits. *MS3* represents *MS2* and *MS3* together.

| Facility                                    | Interface Limit [MW] | MS1 | MS2 | MS3 |
| :------------------------------------------ | :------------------: | :-: | :-: | :-: |
| Adirondack - Porter 230 kV                  | 2058 | -10 | 18  | 8
| Browns Falls - Taylorville 115 kV Circuit 2 | 2065 | 97  | -21 | 73
| Chases Lake - Porter 230 kV                 | 2084 | -15 | 35  | 20
| Browns Falls - Taylorville 115 kV Circuit 1 | 2102 | 67  | -23 | 43
| Moses - Adirondack 230 kV Circuit 2         | 2485 | -19 | 47  | 27
| Moses - Adirondack 230 kV Circuit 1         | 2486 | -19 | 46  | 27



The solutions above can be described by linear functions. Multiple deployments were tested and can be seen on the graphs below. The reason for the non-zero intercept on the 115 kV impact function is due to the need to balance the constraints prior to scaling up.
![Moses 115 kV](ms_115.PNG)

![Moses 230 kV](ms_230.PNG)

***UPNY-SENY***
The two parallel 345 kV circuits between the Leeds and Pleasant Valley substations are traditionally the most critical constraints between Upstate New York and Southeast New York. With the assumption of CPV being in-service prior to 2021, the CPV Valley - Rock Tavern 345 kV circuit has become the more limiting constraint. It is assumed that there is a relatively low cost terminal equipment upgrade at Rock Tavern that could alleviate this constraint and is therefore being considered as less critical. Additional constraints downstream of the UPNY-SENY interface on Orange & Rocklands 138 kV system are monitored and considered for deployments.

Unlike the Moses South transfer analysis, the most limiting constraints to the UPNY-SENY interface are driven by different contingencies. For this reason, the constraints can be alleviated individually as needed. The table below displays three deployments that would results in a net impact of increasing the UPNY-SENY interface by roughly 1000 MW. Deployment *US1* involves a 3.3 ohm deployment on Shoemaker - Chester 138 kV. *US2* involves a net 8.7 Ohm deployment across the Leeds - Pleasant Valley and Athens - Pleasant Valley 345 kV circuits. *US3* involves a 4.6 ohm deployment across the Coopers Corners - Middletown Tap 345 kV circuit.

The deployments of *GN*, *GL*, and *GF* represent 5 ohm injections on NYPA's Gilboa - New Scotland, Gilboa - Leeds, and Gilboa - Fraser 345 kV circuits respectively. Injecting -5 ohms via router application would flip the sign of the impact values.

A deployment that increases the UPNY-SENY transfer limit by 1000 MW would increase the amount of power flowing across Marcy South Series Compensation by roughly 415 MW.  

| Facility                            | Interface Limit [MW] | US1  | US2 | US3 | GN | GL  | GF |
| :---------------------------------- | :------------------: | :--: | :-: | :-: | :-:| :-: | :-: |
| Shoemaker - Chester 138 kV          | 5365                 | 1033 | 0   | 0   | -3 | -27 | 1   |
| CPV Valley - Rock Tavern 345 kV     | 5373                 | 0    | 0   | 0   | -6 | -52 | 0   |
| Leeds - Pleasant Valley 345 kV      | 5614                 | 0    | 732 | 0   | 0  | 48  | -12 |
| Athens - Pleasant Valley 345 kV     | 5675                 | 0    | 681 | 0   | 0  | 48  | -12 |
| Chester - Sugarloaf 138 kV          | 5870                 | 1116 | 0   | 0   | -3 | -29 | -3  |
| Coopers Corners - Middletown 345 kV | 5967                 | 0    | 0   | 405 | -7 | -67 | -16 |
| Ramapo - Tallan 138 kV              | 6223                 | 0    | 0   | 0   | -3 | -34 | -7  |

The solutions above can be described by linear functions. Multiple deployments were tested and can be seen on the graphs below.

![Leeds-PV](us_l-pv.PNG)

![Coop-Middletown](us_cc-mt.PNG)

![Moses 230 kV](us_s-c.PNG)

***Sensitivities***
All sensitivity studies influence the generation dispatch of the load flow model. Without topology changes, Smart Wires' devices have the same influence on power flows. The different sensitivities undoubtedly change interface limits, however the impact functions developed above remain the same.
